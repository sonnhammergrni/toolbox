function flag = issquare(A)
% checks if matrix is square or not

flag = true;
[n,m] = size(A);

if n ~= m
   flag = false; 
end
