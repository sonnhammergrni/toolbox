function document = str2DOMnode(str);
% Takes a well formated xml(like) string and produces a DOMnode. I
% think...
% More info in file:../notebook/read-write-results-in-xml.org

stream = java.io.StringBufferInputStream(str);

factory = javaMethod('newInstance',...
    'javax.xml.parsers.DocumentBuilderFactory');
builder = factory.newDocumentBuilder;

document = builder.parse(stream);
