function B = GramSchmidtOrth(varargin)
% GramSchmidtOrth - Returns a Gram-Schmidt orthonormalized base
%
%   If M has lower rank than number of rows then the base set is completed
%   using the standard orthonormal base (I).
%
%   Input Arguments: (M,nrVectors,prop) 
%   ================ 
%   M = A matrix with one vector per colon. The row size must be non-zero.
%   nrVectors = specifies the number of orthonormal vectors returned.
%   Default is as many as the number of rows of M. 
%   prop = 'off' turns off any warnings
%
%   Output Arguments: 
%   =================
%   B = A matrix consisting of orthonormal colon vectors, where the first
%       vector has the same direction as the first vector in M.

% T. Nordling 22.6.2006
tol = 10^-11; %Do not decrease because then numerical problems can occur
prop = 'on'; %Turn on warnings

M = varargin{1};
[m,n] = size(M); % n = number of vectors, m = length of vectors
nrCol = m;
if nargin == 1, % Change nothing
elseif nargin <= 2,
    for i = 2:nargin,
        if isa(varargin{i},'double'),
            nrCol = varargin{i};
        else
            prop = varargin{i};
        end
    end
    if nrCol > m,
        error('GramSchmidtOrth: The requested number of vectors is higher than the number of rows.')
    end
else
    error('GramSchmidtOrth: Incorrect number of arguments');
end
B = zeros(m,nrCol); % initialize B

if n > 0,
    % First vector has same direction as first one in M
    B(:,1)=M(:,1)/norm(M(:,1));

    temp = zeros(m,1);
    r = 2;
    for k=2:min(n,nrCol),
        temp = M(:,k)- B(:,1:r-1)*(dot(M(:,k)*ones(1,r-1), B(:,1:r-1)))';
        if sum(abs(temp)) > tol,
            B(:,r) = temp/norm(temp);
            if rank(B) == r, %Otherwise last vector was linearly dependent
                r = r+1;
            end
        end
    end
else
    r = 1;
end

% Warns that given matrix has lower rank than length of vectors and
% completes the basis
if r <= nrCol,
    if strcmp(prop,'off'), %No warning
    else
        disp(['GramSchmidtOrth: Given matrix has rank ' num2str(r-1)])
    end
    
    % Trys to complete base using standard orthonormal base
    M = eye(m);
    for k=1:nrCol,
        temp = M(:,k)- B(:,1:r-1)*(dot(M(:,k)*ones(1,r-1), B(:,1:r-1)))';
        if sum(abs(temp)) > tol,
            B(:,r) = temp/norm(temp);
            if rank(B) == r, %Otherwise last vector was linearly dependent
                r = r+1;
            end
        end
    end
    
    if strcmp(prop,'off'), %No warning
    else
        if r-1 == nrCol,
            disp('Base set completed using standard orthonormal base.')
        else
            disp('Base set completed using random vectors.')
        end
    end
    
    % Completes basis using random vectors
    while r <= nrCol,
        cand = rand(m,1);
        temp = cand- B(:,1:r-1)*(dot(cand*ones(1,r-1), B(:,1:r-1)))';
        if sum(abs(temp)) > tol,
            B(:,r) = temp/norm(temp);
            if rank(B) == r, %Otherwise last vector was linearly dependent
                r = r+1;
            end
        end
    end
end
B = B(:,1:nrCol);

% Checks that linearly independent vectors have been produced
if rank(B) < nrCol,
    error('GramSchmidtOrth: Retuned matrix is singular'); 
end