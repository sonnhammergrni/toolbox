function out = mand(in,dim);
% Matrix 'and' function
% out = mand(in[,dim]);
%
% Calcualtes the and operation along the given dimension.
%

if ~exist('dim','var')
    dim = 1;
end
ndim = length((size(in)));
if ndim < dim
    error(' Index exceeds matrix dimensions.\n\n input have no dimension %d',dim)
end


indim = ['in(:',repmat(',:',1,ndim-1),')'];
indim(2+dim*2) = 'i';

i=1;
tmp = eval(indim);
i=2;
out = and(tmp,eval(indim));

for i=3:size(in,dim)
    out = and(out,eval(indim));
end