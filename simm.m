function L = simm(A, p, tids)
%%%%%
% This script simulates transcript changes for a given GRN and 
% perturbation vector using the basic control theory model.
% The output is a figure and a matrix L that has all the transcript 
% values for both genes.
%
% A: network matrix (n x n)
% p: perturbation vector (n x 1)
% tids: number of time points
%
% L: output data matrix (n x tids); states at each time point
%%%%%
clf; % Clear figure window
n = size(A,1);
L = zeros(n,tids);
x = zeros(n,1); % Starting state is zero 
L(:,1) = x;

% Simulate
for t = 1:tids
    dx = A*x + p;
    x = dx*0.01 + x;
    L(:,t+1) = x;
end

% Make legend
Genes = cell(n,1);
for g = 1:n
    Genes{g} = ['x' int2str(g)];
end

% Plot
plot(0:tids,L');
legend(Genes, 'Location', 'EastOutside');
