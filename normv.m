function varargout = normv(varargin)
%NORMV   Matrix or vector norm. (Including some special norms.)
%   For matrices...
%     NORMV(X) is the largest singular value of X, max(svd(X)).
%     NORMV(X,2) is the same as NORM(X).
%     NORMV(X,1) is the 1-norm of X, the largest column sum,
%                     = max(sum(abs(X))).
%     NORMV(X,inf) is the infinity norm of X, the largest row sum,
%                     = max(sum(abs(X'))).
%     NORMV(X,'fro') is the Frobenius norm, sqrt(sum(diag(X'*X))).
%     NORMV(X,'vec') is the lenght of each colon vector of X,
%                     = sqrt(diag(X'*X)).
%     NORMV(X,'l1') is the l1-norm of X, the element sum,
%                     = sum(sum(abs(X))).
%     NORMV(X,P) is available for matrix X only if P is 1, 2, inf or 'fro'.
%
%   For vectors...
%     NORMV(V,P) = sum(abs(V).^P)^(1/P).
%     NORMV(V) = norm(V,2).
%     NORMV(V,inf) = max(abs(V)).
%     NORMV(V,-inf) = min(abs(V)).
%
%   For arrays...
%     NORMV(X,...) Same options as for matrices and does it on the matrices,
%     i.e. X(:,:,i,j,k,...) returning and array Y(:,i,j,k,...)
%
%   See also NORM, COND, RCOND, CONDEST, NORMEST, HYPOT.

% Information:
% ============
% TN's Matlab Toolbox
% Copyright (C) 2006 Torbj�rn Nordling, tn@kth.se
% 
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

X = varargin{1};
if ndims(X) > 2, %Array given
    arraysize = size(X);
    X = reshape(X,[arraysize([1 2]) prod(arraysize(3:end))]); %Reshapes column by column
    temp = normv(X(:,:,1), varargin{2:end});
    Y = zeros(numel(temp),prod(arraysize(3:end))); %Allocate vector
    for i = 1:prod(arraysize(3:end)),
        Y(:,i) = normv(X(:,:,i), varargin{2:end});
    end
    if numel(arraysize) == 3,
        varargout{1} = reshape(Y, [numel(temp) arraysize(3:end)]); %Array size must have two elements
    else
        varargout{1} = reshape(Y, [numel(temp) arraysize(3:end)]);
    end
else %Matrix or vector given
    if nargin == 2,
        % Two inputs given so must check for special ones
        switch varargin{2},
            case 'vec'
                varargout{1} = sqrt(diag(X'*X));
            case 'l1'
                varargout{1} = sum(sum(abs(X)));
            otherwise
                varargout = {norm(varargin{:})};
        end
    else
        % Only one input is given so standard norm is used
        varargout = {norm(varargin{:})};
    end
end